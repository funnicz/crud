<div class="container">
	

	<div class="row">
		<div class="col-md-6">
			<div class="card">
			  <div class="card-header">
			    Form +Add Mahasiswa
			  </div>
			  <div class="card-body">
			  	<?php  if(validation_errors() ): ?>
				 <div class="alert alert-warning" role="alert">
					  <?=validation_errors(); ?>
				</div>
			<?php endif; ?>
			  
			    <form action="" method="post">
					<div class="form-group">
						<label for="nama">Nama</label>
	  				 	<input type="text" name="nama" class="form-control" id="nama" placeholder="Albus Severus Potter">	 
	  				</div>
	  				<div class="form-group">
						<label for="nim">NIM</label>
	  				 	<input type="text" name="nim" class="form-control" id="nim" placeholder="20*******">	 
	  				</div>
	  				<div class="form-group">
					  	<label for="exampleFormControlInput1">Email</label>
					   	<input type="text" name="email" class="form-control" id="email" placeholder="name@example.com">
					 </div>
					<div class="form-group">
					   <label for="exampleFormControlSelect1">Jurusan</label>
					    <select class="form-control" id="jurusan" name="jurusan">
					      <option value="IT">IT</option>
					      <option value="SI">SI</option>
					      <option value="GC">GC</option>
					      <option value="BC">BC</option>
					      <option value="DKV">DKV</option>					      
					    </select>
					</div>
					<button type="submit" name="tambah" class="btn btn-primary float-right">+Add</button>
				</form>
			  </div>
			</div>
		</div>
	</div>



</div>