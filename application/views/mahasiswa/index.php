<div class="container">


	<?php if($this->session->flashdata('flash')): ?>
<div class="row mt-3">
	<div class="col-md-6">
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <strong>SELAMAT!</strong> Data Mahasiswa Berhasil Ditambahkan. <?= $this->session->flashdata('flash'); ?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
		</div>
	</div>
</div>
<?php endif; ?>

	<div class="row mt-3">
		<div class="col-md-6">
			<a href="<?= base_url(); ?>mahasiswa/tambah" class="btn btn-primary">+Add Mahasiswa</a>
		</div>
	</div>

	<div class="row mt-3">
		<div class="col-md-6">
			<h2>Daftar Mahasiswa</h2>
			<ul class="list-group">
				<?php foreach ($mahasiswa as $mhs) : ?>
				  <li class="list-group-item">
				  	<?= $mhs['nama']; ?>
				  	<a href="<?= base_url(); ?>mahasiswa/hapus/<?= $mhs['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('Yakin?');">Delete</a>
				  	<a href="<?= base_url(); ?>mahasiswa/detail/<?= $mhs['id']; ?>" class="badge badge-primary float-right">Detail</a>
				  	<a href="<?= base_url(); ?>mahasiswa/edit/<?= $mhs['id']; ?>" class="badge badge-success float-right">Edit</a>
				  </li>
				<?php endforeach; ?>
			</ul>
			 
		</div>	
	</div>
</div>